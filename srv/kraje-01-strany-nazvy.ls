require! {
  fs
  request
  xml2js
  "iconv-lite": iconv
}

(err, res, body) <~ request {url: "http://www.volby.cz/pls/kz2012/vysledky", encoding: null}
body = iconv.decode body, "iso-8859-2"
body .= toString!
(err, xml) <~ xml2js.parseString body
strany = {}
uids = {}
xml.VYSLEDKY.KRZAST.forEach ->
  it.STRANA.forEach ->
    nazev = it.$.NAZ_STR
    uid = it.$.VSTRANA
    strany[it.$.KSTRANA] = {nazev, uid}
    uids[uid] = nazev
metas =
  "1": color: \#FEE300 name: "Křesťanská a demokratická unie - Československá strana lidová" zkratka: "KDU ČSL"
  "5": color: \#0FB103 name: "Strana zelených" zkratka: "SZ"
  "7": color: \#FEA201 name: "Česká strana sociálně demokratická" zkratka: "ČSSD"
  "47": color: \#F40000 name: "Komunistická strana Čech a Moravy" zkratka: "KSČM"
  "53": color: \#1C76F0 name: "Občanská demokratická strana" zkratka: "ODS"
  "144": color: \#66E2D8 name: "Věci veřejné" zkratka: "VV"
  "156": color: \#B55E01 name: "Dělnická strana sociální spravedlnosti" zkratka: "DSSS"
  "192": color: \#66E2D8 name: "HLAVU VZHŮRU - volební blok" zkratka: "HLAVU VZHŮRU"
  "720": color: \#504E4F name: "Česká pirátská strana" zkratka: "Piráti"
  "659": color: \#B560F3 name: "TOP 09" zkratka: "TOP 09"
  "764": color: \#990422 name: "LEV 21 - Národní socialisté" zkratka: "LEV 21"
  "768": color: \#5434A3 name: "ANO 2011" zkratka: "ANO 2011"
  "792": color: \#B3C382 name: "Úsvit přímé demokracie" zkratka: "Úsvit"

for id, strana of strany
  meta = metas[strana.uid]
  line = [id, strana.nazev, strana.uid, meta?name, meta?zkratka, meta?color]
    .join "\t"
  console.log line
# for uid, nazev of uids
#   console.log "#uid\t#nazev"
