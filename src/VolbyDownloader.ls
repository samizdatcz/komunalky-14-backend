require! {
  async
  "iconv-lite": iconv
  xml2js
  request
  moment
  fs
  events
}
okresy_csu = fs.readFileSync "#__dirname/../data/okresy_csu.txt"
  .toString!
  .replace /\r/g ''
  .split "\n"
  .filter -> it.length
okresy_nuts = fs.readFileSync "#__dirname/../data/okresy_nuts.txt"
  .toString!
  .replace /\r/g ''
  .split "\n"
  .filter -> it.length

module.exports = class VolbyDowloader extends events
  minimumInterval: 10
  (@config) ->
    @sources = []
    if @config.addr.senat
      @sources.push do
        url: @config.addr.senat
        interval: 60
        type: \senat-vysledky
        short: \senat-vysledky
    if @config.addr.komunalky
      okresy_nuts.forEach ~>
        @sources.push do
          url: "#{@config.addr.komunalky}/vysledky_obce_okres?nuts=#{it}"
          interval: 60
          type: \komunalky-obec
          short: "komunalky-obec-#{it}"
    if @config.addr.kraje
      okresy_nuts
        .filter -> it.length > 2 and it isnt "CZ0100" # v Praze se nevoli
        .forEach ~>
          @sources.push do
            url: "#{@config.addr.kraje}/vysledky_okres?nuts=#{it}"
            nuts: it
            interval: 60
            type: \kraj
            short: "kraj-okres-#{it}"
      @sources.push do
        url: "#{@config.addr.kraje}/vysledky_kandid"
        interval: 60
        type: \kraj-kandid
        short: "kraj-kandid"

  start: ->
    @sources.forEach (source, index) ~>
      @downloadSourceIn source, index

  downloadSource: (source) ->
    opts =
      uri: source.url
      encoding: null
    # console.log "Downloading #{source.short}"
    (err, response, body) <~ request.get opts
    # console.log "Downloaded #{source.short}"
    if err or not body.length
      if err
        console.error "Error downloading #{opts.uri}", err
      else
        console.error "Error downloading #{opts.uri}: zero content length"
      @downloadSourceIn source, 15
      return
    data = iconv.decode body, "iso-8859-2"
    (err, xml) <~ xml2js.parseString data
    try
      throw err if err
      root = null
      for rootName, rootContent of xml
        root = rootContent
      if root.$.DATUM_CAS_GENEROVANI
        [date, time] = root.$.DATUM_CAS_GENEROVANI.split "T"
        generated = moment do
          date + " " + time + " +0200"
          "YYYY-MM-DD HH:mm:ss Z"
      else
        date = root.$.DATUM_GENEROVANI
        time = root.$.CAS_GENEROVANI
        generated = moment do
          date + " " + time + " +0200"
          "DD/MM/YYYY HH:mm:ss Z"
      difference = Date.now! - generated.valueOf!
      difference /= 1000
      difference = Math.floor difference
      interval = source.interval - difference
      if isNaN interval
        interval = 30
      if interval < @minimumInterval then interval = @minimumInterval
      niceTime = moment!format "YYYY-MM-DD-HH-mm-ss"
      @downloadSourceIn source, interval + 2
      # console.log "Parsed #{source.short}. Next download in #{interval + 2}"
      index = @sources.indexOf source
      @emit do
        source.type
        xml
        data
        index
        @sources[index]
      fs.writeFile "#__dirname/../data/output/#{niceTime}-#{source.short}", data

    catch ex
      console.error "Error parsing #{opts.uri}", ex
      @downloadSourceIn source, 15

  downloadSourceIn: (source, seconds) ->
    fn = ~> @downloadSource source
    console.log "In #source, d #seconds"
    setTimeout fn, seconds * 1e3
