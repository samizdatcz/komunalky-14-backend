maxInt = 65500
module.exports =
  encode: (int) ->
    if int == null or not (int >= 0)
      int = -1
    out = ""
    do
      intToUse = Math.min int, maxInt
      intToEncode = intToUse + 33
      out += String.fromCharCode intToEncode
      int -= intToUse
    while int
    out

  decode: (str, position = 0) ->
    int = 0
    do
      decodedInt = str.charCodeAt position
      decodedInt -= 33
      if decodedInt < 0
        return null
      int += decodedInt
      position++
    while decodedInt == maxInt
    int

  encodeArray: (arr) ->
    out = ""
    for field in arr
      out += this.encode field
    out

  decodeArray: (str, position = 0) ->
    lastPosition = str.length - 1
    out = while position <= lastPosition
      int = this.decode str, position
      position += 1 + Math.floor (int || 0) / maxInt
      int
    out
