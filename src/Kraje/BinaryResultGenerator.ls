require! {
  "../binaryString.ls"
}
statsFields = <[okrsky_celkem okrsky_zprac zapsani_volici vydane_obalky platne_hlasy]>
module.exports =
  encode: (obceArray) ->
    stranyAssoc = {}
    stranyArray = []
    for obec in obceArray
      for strana in obec.strany
        if stranyAssoc[strana.id] is void
          len = stranyArray.push strana.id
          stranyAssoc[strana.id] = len - 1

    out = ""
    out += binaryString.encodeArray stranyArray
    out += "\n"
    blankArray = stranyArray.map -> null
    for obec in obceArray
      line = obec.id
      for statsField in statsFields
        line += binaryString.encode obec.stats[statsField]
      obceStrany = blankArray.slice!
      for {id, hlasy} in obec.strany
         position = stranyAssoc[id]
         obceStrany[position] = hlasy
      line += binaryString.encodeArray obceStrany
      out += line + "\n"
    out

  decode: (str) ->
    [strany, ...obce] = str.split "\n"
    stranyIds = for i in [0 til strany.length]
      binaryString.decode strany[i]
    for obec in obce
      obecId = parseInt do
        obec.substr 0, 6
        10
      values = binaryString.decodeArray obec, 6
      stats = {}
      for field, index in statsFields
        stats[field] = values[index]
      values .= slice statsFields.length
      strany = for stranaId, index in stranyIds
        id = stranaId
        hlasy = values[index]
        continue if hlasy is null
        {id, hlasy}

      {id:obecId, stats, strany}
