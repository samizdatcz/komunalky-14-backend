require! {
  fs
  dhondt
}
krajeToObce = {}
obceToKraje = {}
obceKraje = fs.readFileSync "#__dirname/../../data/obce_kraje.txt"
  .toString!
  .split "\n"
  .filter -> it.length > 2
krajeToMandaty = {}
krajeMeta = fs.readFileSync "#__dirname/../../data/kraje_meta.tsv"
  .toString!
  .split "\n"
  .slice 1
  .filter -> it.length > 2
  .map ->
    [nuts, mandaty, cuzkId] = it.replace "\r" "" .split "\t"
    mandaty = parseInt mandaty, 10
    {nuts, mandaty}
for krajMeta in krajeMeta
  krajeToMandaty[krajMeta.nuts] = krajMeta.mandaty

for obec in obceKraje
  [obec, kraj] = obec.replace "\r" "" .split "\t"
  if krajeToObce[kraj] is void
    krajeToObce[kraj] = []
  krajeToObce[kraj].push obec
  obceToKraje[obec] = kraj
module.exports =
  setResultFile: (@resultFile) ->
  setKandidFile: (@kandidFile) ->

  getObceInKraj: (nuts) ->
    out = for obec in krajeToObce[nuts]
      continue if @resultFile[obec] is void
      @resultFile[obec]
    out

  getTotals: ->
    stats =
      okrsky_celkem: 0
      okrsky_zprac: 0
      zapsani_volici: 0
      vydane_obalky: 0
      platne_hlasy: 0
    allStranyAssoc = {}
    allStrany = []
    krajeAssoc = {}
    kraje = []
    for obecId, obecData of @resultFile
      krajId = obceToKraje[obecId]
      if krajeAssoc[krajId] is void
        krajeAssoc[krajId] =
          id: krajId
          stats:
            okrsky_celkem: 0
            okrsky_zprac: 0
            zapsani_volici: 0
            vydane_obalky: 0
            platne_hlasy: 0
          stranyAssoc: {}
          strany: []
        kraje.push krajeAssoc[krajId]
      kraj = krajeAssoc[krajId]
      for field, value of obecData.stats
        stats[field] += value
        kraj.stats[field] += value
      for strana in obecData.strany
        if allStranyAssoc[strana.id] is void
          allStranyAssoc[strana.id] =
            id: strana.id
            hlasy: 0
            mandaty: 0
          allStrany.push allStranyAssoc[strana.id]
        allStranyAssoc[strana.id].hlasy += strana.hlasy
        if kraj.stranyAssoc[strana.id] is void
          kraj.stranyAssoc[strana.id] = {id: strana.id, hlasy: 0, mandaty: 0}
          kraj.strany.push kraj.stranyAssoc[strana.id]
        krajStrana = kraj.stranyAssoc[strana.id]
        krajStrana.hlasy += strana.hlasy
    for kraj in kraje
      if krajeToMandaty[kraj.id]
        kraj.strany.forEach -> it.mandaty = 0
        krajTotalVotes = 0
        for strana in kraj.strany
          krajTotalVotes += strana.hlasy
        if krajTotalVotes
          votesLimit = krajTotalVotes * 0.05
          passingStrany = kraj.strany.filter -> it.hlasy >= votesLimit
          mandateCount = krajeToMandaty[kraj.id]
          votes = passingStrany.map (.hlasy)
          mandates = passingStrany.map (.mandaty)
          mandatesAwarded = 0
          while mandatesAwarded < mandateCount
            {winningIndex, winningScore} = getRoundWinner votes, mandates, 1.42
            mandates[winningIndex]++
            passingStrany[winningIndex].mandaty++
            passingStrany[winningIndex].dhondtScore = winningScore
            mandatesAwarded++
          passingStrany[winningIndex].lastMandate = true
          {winningIndex, winningScore} = getRoundWinner votes, mandates, 1.42
          passingStrany[winningIndex].firstLoser = true
      delete kraj.stranyAssoc

    {stats, strany:allStrany, kraje}


getRoundWinner = (votes, mandates, base) ->
  highestIndex = -1
  highestScore = -Infinity
  for voteCount, index in votes
    mandateCount = mandates[index]
    divider = if mandateCount then that + 1 else base
    score = voteCount / divider
    if score > highestScore
      highestScore = score
      highestIndex = index
  {winningIndex: highestIndex, winningScore: highestScore}
