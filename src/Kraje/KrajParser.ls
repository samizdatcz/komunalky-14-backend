module.exports.parse = (xml) ->
  try
    out = {}
    root = xml.VYSLEDKY_OKRES
    out.obce = for obec in root.OBEC
      strany = []
      if obec.HLASY_STRANA
        strany = for strana in obec.HLASY_STRANA
          id: parseInt strana.$.KSTRANA, 10
          hlasy: parseInt strana.$.HLASY, 10
      id : parseInt obec.$.CIS_OBEC, 10
      stats :
        okrsky_celkem: parseInt obec.UCAST.0.$.OKRSKY_CELKEM
        okrsky_zprac: parseInt obec.UCAST.0.$.OKRSKY_ZPRAC
        zapsani_volici: parseInt obec.UCAST.0.$.ZAPSANI_VOLICI
        vydane_obalky: parseInt obec.UCAST.0.$.VYDANE_OBALKY
        platne_hlasy: parseInt obec.UCAST.0.$.PLATNE_HLASY
      strany : strany

    out
  catch e
    console.error "Trouble parsing Kraj XML" e
    null
