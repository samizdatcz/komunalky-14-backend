module.exports.parse = (xml) ->
  try
    out = {}
    root = xml.VYSLEDKY_KANDID
    out.kraje = for kraj in root.KRZAST
      krajCuzkId = parseInt kraj.$.CIS_KRZAST, 10
      kandidati = for kandidat in kraj.KANDIDATI.0.KANDIDAT
        kstrana = parseInt kandidat.$.KSTRANA, 10
        porcislo = parseInt kandidat.$.PORCISLO, 10
        hlasy = parseInt kandidat.$.HLASY, 10
        {kstrana, porcislo, hlasy}
      {krajCuzkId, kandidati}
    out
  catch e
    console.error "Trouble parsing Kandid XML" e
    null
