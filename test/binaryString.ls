require! {
  'expect.js': expect
  '../src/binaryString.ls'
}

describe \binaryString (_) ->
  it 'should encode and decode arrays' ->
    arr = [null, 1000, 99999, 1000000, 0]
    encoded = binaryString.encodeArray arr
    decoded = binaryString.decodeArray  encoded
    expect arr .to.eql decoded

  it 'should encode and decode' ->
    i = 0
    do
      encoded = binaryString.encode i
      expect encoded .to.be.a \string
      expect encoded.length .to.be 1
      decoded = binaryString.decode encoded
      expect decoded .to.equal i
      i++
    while i < 1000

  it 'should encode and decode large numbers' ->
    numbers =
      1000
      2000
      10000
      100000
      1000000
      10000000
    for number in numbers
      encoded = binaryString.encode number
      expect encoded .to.be.a \string
      decoded = binaryString.decode encoded
      expect decoded .to.equal number

  it 'should encode and decode nulls and undefineds' ->
    encoded = binaryString.encode null
    decoded = binaryString.decode encoded
    expect decoded .to.equal null

    encoded = binaryString.encode void
    decoded = binaryString.decode encoded
    expect decoded .to.equal null

  it 'should decode strings at arbitary positions' ->
    encoded = (binaryString.encode 1) + (binaryString.encode 2)
    expect binaryString.decode encoded, 0 .to.equal 1
    expect binaryString.decode encoded, 1 .to.equal 2
