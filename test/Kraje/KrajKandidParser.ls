require! {
  'expect.js': expect
  xml2js
  fs
  '../../src/Kraje/KrajKandidParser.ls'
}

describe \krajKandidParser (_) ->
  xml = null
  before (done) ->
    data = fs.readFileSync "#__dirname/../data/vysledky_kandid.xml" .toString!
    (err, _xml) <~ xml2js.parseString data
    xml := _xml
    done!

  it 'should parse okres-level results' ->
    out = KrajKandidParser.parse xml
    expect out .to.have.property \kraje
    kraje = out.kraje
    expect kraje .to.be.an \array
    kraj = kraje.0
    expect kraje .to.be.an \object
    expect kraj .to.have.property \krajCuzkId 1
    expect kraj .to.have.property \kandidati
    kandidati = kraj.kandidati
    expect kandidati .to.be.an \array
    kandidat = kandidati.0
    expect kandidat .to.be.an \object
    expect kandidat .to.have.property \kstrana 3
    expect kandidat .to.have.property \porcislo 1
    expect kandidat .to.have.property \hlasy 441
    fs.writeFileSync do
      "#__dirname/../data/vysledky_kandid.json"
      JSON.stringify out, 1, 2
