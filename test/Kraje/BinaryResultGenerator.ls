require! {
  fs
  'expect.js': expect
  '../../src/Kraje/BinaryResultGenerator.ls'
}

describe \BinaryResultGenerator (_) ->
  data = []
  testedObce = <[ 548812 548804 548791 ]>
  before ->
    kraje = JSON.parse do
      fs.readFileSync "#__dirname/../data/kraje.json"
    # testedObce := []
    # for obec of kraje
    #   testedObce.push obec
    data := testedObce.map ->
      kraje[it]


  it "should encode and decode data" ->
    encoded = BinaryResultGenerator.encode data
    expect encoded .to.be.a \string
    # fs.writeFileSync "#__dirname/../data/kraje.txt", encoded
    decoded = BinaryResultGenerator.decode encoded
    expect decoded .to.be.an \array
    expect decoded[0] .to.eql data[0]
    # for obec, index in decoded
    #   if obec.id == 546135
    #     console.log decoded[index]
