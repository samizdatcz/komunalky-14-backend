require! {
  'expect.js': expect
  "../../src/VolbyDownloader.ls"
  "../../src/Kraje/KrajParser.ls": parser
  fs
}
config =
  addr:
    kraje: \http://www.volby.cz/pls/kz2012


downloader = new VolbyDownloader config
out = {}
downloader.on \kraj-kandid (xml, data) ->
  out = parser.parse xml
  fs.writeFileSync do
    "#__dirname/../data/kraj-kandid.json"
    JSON.stringify out, 1, 2

downloader.on \kraj (xml, data) ->
  okres = parser.parse xml
  for obec in okres.obce
    out[obec.id] = obec
  fs.writeFileSync do
    "#__dirname/../data/kraje.json"
    JSON.stringify out, 1, 2
downloader.start!
