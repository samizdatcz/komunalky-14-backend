require! {
  'expect.js': expect
  xml2js
  fs
  '../../src/Kraje/KrajParser.ls'
}

describe \krajParser (_) ->
  xml = null
  before (done) ->
    data = fs.readFileSync "#__dirname/../data/kraj2101.xml" .toString!
    (err, _xml) <~ xml2js.parseString data
    xml := _xml
    done!

  it 'should parse okres-level results' ->
    out = KrajParser.parse xml
    expect out .to.have.property \obce
    expect out.obce .to.be.an \array
    expect out.obce .to.not.be.empty!
    expect out.obce .to.have.length 114
    obec = out.obce[0]
    expect obec .to.have.property \id
    expect obec.id .to.equal 513482
    expect obec.stats .to.be.an \object
    expect obec.stats.okrsky_celkem .to.equal 1
    expect obec.stats.okrsky_zprac .to.equal 1
    expect obec.stats.zapsani_volici .to.equal 151
    expect obec.stats.vydane_obalky .to.equal 57
    expect obec.stats.platne_hlasy .to.equal 51
    expect obec.strany .to.be.an \array
    expect obec.strany .to.have.length 13
    strana = obec.strany.0
    expect strana .to.be.an \object
    expect strana.id .to.equal 3
    expect strana.hlasy .to.equal 3
