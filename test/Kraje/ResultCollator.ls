require! {
  'expect.js': expect
  fs
  '../../src/Kraje/ResultCollator.ls'
}

describe \ResultCollator (_) ->
  data = null

  before ->
    data := JSON.parse fs.readFileSync "#__dirname/../data/kraje.json"

  it 'should filter kraj-level counties' ->
    ResultCollator.setResultFile data
    obce = ResultCollator.getObceInKraj \CZ020
    expect obce.length .to.equal 1145

  it 'should provide totals' ->
    totals = ResultCollator.getTotals!
    expect totals .to.have.property \stats
    expect totals.stats .to.have.property \okrsky_celkem 13670
    expect totals.stats .to.have.property \okrsky_zprac 13670
    expect totals.stats .to.have.property \zapsani_volici 7491806
    expect totals.stats .to.have.property \vydane_obalky 2763464
    expect totals.stats .to.have.property \platne_hlasy 2637115
    expect totals .to.have.property \strany
    expect totals.strany .to.be.an \array
    for strana in totals.strany
      if strana.id == 70
        expect strana.hlasy .to.equal 324081
    expect totals .to.have.property \kraje
    expect totals.kraje .to.be.an \array
    expect totals.kraje.0 .to.have.property \id "CZ072"
    expect totals.kraje.0 .to.have.property \strany
    expect totals.kraje.0.strany .to.be.an \array
    expect totals.kraje.0.strany.0 .to.have.property \id 5
    expect totals.kraje.0.strany.0 .to.have.property \hlasy 378
    expect totals.kraje.0.strany.0 .to.have.property \mandaty 0
    expect totals.kraje.0.strany.3 .to.have.property \hlasy 13488
    expect totals.kraje.0.strany.3 .to.have.property \mandaty 4
    expect totals.kraje.0 .to.have.property \stats
    fs.writeFileSync "#__dirname/../data/kraje_totals.json", JSON.stringify totals.kraje#, 1, 2
